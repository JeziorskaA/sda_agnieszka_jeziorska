/*
 * Figura.hpp
 *
 *  Created on: 25.03.2017
 *      Author: Agnieszka
 */
#ifndef FIGURA_HPP_
#define FIGURA_HPP_
class Figura
{
public:
	virtual float pole()=0;
	virtual ~Figura();
};
#endif /* FIGURA_HPP_ */

