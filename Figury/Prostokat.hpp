/*
 * Prostokat.hpp
 *
 *  Created on: 25.03.2017
 *      Author: Agnieszka
 */

#ifndef PROSTOKAT_HPP_
#define PROSTOKAT_HPP_
#include "Figura.hpp"
#include<iostream>
class Prostokat: public Figura
{
private:
	float mA;
	float mB;
public:
	Prostokat();
	Prostokat(float a, float b);
	float pole();
	virtual ~Prostokat();
};
#endif /* PROSTOKAT_HPP_ */
