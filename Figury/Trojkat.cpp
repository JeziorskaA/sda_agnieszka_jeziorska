/*
 * Trojkat.cpp
 *
 *  Created on: 25.03.2017
 *      Author: Agnieszka
 */
#include "Trojkat.hpp"
Trojkat::Trojkat() :
		mA(0.0), mH(0.0)
{
}
Trojkat::Trojkat(float a, float h) :
		mA(a), mH(h)
{
}
float Trojkat::pole()
{
	float poleT = 0.0;
	poleT = 0.5 * (mA * mH);
	std::cout << "Pole tr�jk�ta wynosi: " << poleT << std::endl;
	return poleT;
}
Trojkat::~Trojkat()
{
}
;

