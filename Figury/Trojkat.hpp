/*
 * Trojkat.hpp
 *
 *  Created on: 25.03.2017
 *      Author: Agnieszka
 */

#ifndef TROJKAT_HPP_
#define TROJKAT_HPP_
#include "Figura.hpp"
#include <iostream>
class Trojkat: public Figura
{
private:
	float mA;
	float mH;
public:
	Trojkat();
	Trojkat(float a, float h);
	float pole();
	virtual ~Trojkat();

};
#endif /* TROJKAT_HPP_ */
