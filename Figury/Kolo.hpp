/*
 * Kolo.hpp
 *
 *  Created on: 25.03.2017
 *      Author: Agnieszka
 */

#ifndef KOLO_HPP_
#define KOLO_HPP_
#include "Figura.hpp"
class Kolo: public Figura
{
private:
	float mR;
	const float mPI = 3.14;
public:
	Kolo();
	Kolo(float promien);
	float pole();
	virtual ~Kolo();
};
#endif /* KOLO_HPP_ */
