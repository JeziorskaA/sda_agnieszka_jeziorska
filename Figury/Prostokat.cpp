/*
 * Prostokat.cpp
 *
 *  Created on: 25.03.2017
 *      Author: Agnieszka
 */
#include "Prostokat.hpp"

Prostokat::Prostokat() :
		mA(0.0), mB(0.0)
{
}
Prostokat::Prostokat(float a, float b) :
		mA(a), mB(b)
{
}
float Prostokat::pole()
{
	float poleP = 0.0;
	poleP = mA * mB;
	std::cout << "Pole prostokąta wynosi: " << poleP << std::endl;
	return poleP;
}
Prostokat::~Prostokat()
{
}
;

