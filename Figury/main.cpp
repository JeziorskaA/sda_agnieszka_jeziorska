/*
 * main.cpp
 *
 *  Created on: 25.03.2017
 *      Author: Agnieszka
 */
#include "Figura.hpp"
#include "Kolo.hpp"
#include "Prostokat.hpp"
#include "Trojkat.hpp"

int main()
{
	Figura *figura1[3];
	figura1[0] = new Kolo(2.5);
	figura1[1] = new Prostokat(4.8, 6.1);
	figura1[2] = new Trojkat(7.3, 4.9);

	for (int i = 0; i < 3; i++)
	{
		figura1[i]->pole();
	}
	for (int i = 0; i < 3; i++)
	{
		delete figura1[i];
	}
	return 0;
}

