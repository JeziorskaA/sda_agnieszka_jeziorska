/*
 * Kolo.cpp
 *
 *  Created on: 25.03.2017
 *      Author: Agnieszka
 */
#include <iostream>
#include "Kolo.hpp"
Kolo::Kolo() :
		mR(0.0)
{
}
Kolo::Kolo(float promien) :
		mR(promien)
{
}
float Kolo::pole()
{
	float poleK = 0.0;
	poleK = mPI * mR * mR;
	std::cout << "Pole ko�a wynosi: " << poleK << std::endl;
	return poleK;
}
Kolo::~Kolo()
{
}
;

