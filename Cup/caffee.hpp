/*
 * caffee.hpp
 *
 *  Created on: 08.05.2017
 *      Author: Agnieszka
 */

#ifndef CAFFEE_HPP_
#define CAFFEE_HPP_

#include "liquid.hpp"

class caffee: public liquid {
protected:
	int mCafeine;
public:
	caffee() :
		mCafeine(0) {
	}
	caffee(int amount, int cafeine)
	:liquid(amount)
	,mCafeine(cafeine)
	{}

	virtual void add(int amount)
	{
		mAmount+= amount;

	}
	virtual void remove(int amount)
	{
		mAmount = (mAmount > amount) ? mAmount - amount :0;
	}
	virtual void removeAll()
	{
		mAmount = 0;
	}

	virtual ~caffee() {
	}

	int getCafeine() const {
		return mCafeine;
	}
};

#endif /* CAFFEE_HPP_ */
