/*
 * liquid.hpp
 *
 *  Created on: 08.05.2017
 *      Author: Agnieszka
 */

#ifndef LIQUID_HPP_
#define LIQUID_HPP_
class liquid {
protected:
	int mAmount;
public:
	liquid() :
			mAmount(0) {
	}
	liquid(int amount) :
			mAmount(amount) {
	}
	virtual void add(int amount) = 0;
	virtual void remove(int amount) = 0;
	virtual void removeAll() = 0;

		virtual ~liquid() {}

		int getAmount() const {
		return mAmount;
	}

};
#endif /* LIQUID_HPP_ */
