/*
 * decaff.hpp
 *
 *  Created on: 08.05.2017
 *      Author: Agnieszka
 */

#ifndef DECAFF_HPP_
#define DECAFF_HPP_
#include "caffee.hpp"
class decaff: public caffee {
public:
	decaff(int amount) :
			caffee(amount, 0) {
	}
	void add(int amount) {
		mAmount += amount;

	}
	void remove(int amount) {
		mAmount = (mAmount > amount) ? mAmount - amount : 0;
	}
	void removeAll() {
		mAmount = 0;
	}

};

#endif /* DECAFF_HPP_ */
