/*
 * cup.hpp
 *
 *  Created on: 08.05.2017
 *      Author: Agnieszka
 */

#ifndef CUP_HPP_
#define CUP_HPP_
#include "liquid.hpp"
#include "milk.hpp"
#include <iostream>
using namespace std;
#include <cstdlib>
class cup {
private:

	liquid **mContent;
	size_t mSize;

	int calculateLiquidAmount() {
		int liquidAmount = 0;
		if (mSize == 0) {

		} else {

			for (size_t i = 0; i < mSize; i++) {
				liquidAmount = mContent[i]->getAmount();
			}
		}
		return liquidAmount;
	}

	void resize() {
		if (mSize == 0) {
			mContent = new liquid *[++mSize];

		} else {
			liquid ** tmp = new liquid *[++mSize];
			for (size_t i = 0; i < mSize - 1; i++) {
				tmp[i] = mContent[i];
			}
			delete[] mContent;
			mContent = tmp;
		}
	}

public:
	cup() :
			mContent(0), mSize(0) {
	}
	void add(const milk &liquid) {
		resize();
		mContent[mSize - 1] = new milk(liquid);
	}
	void add(const caffee &liquid) {
			resize();
			mContent[mSize - 1] = new caffee(liquid);
		}

	void wypisz() {
		for (size_t i = 0; i < mSize; i++) {
			cout << mContent[i]->getAmount() << endl;
		}
	}
	void add(liquid *amount) {
		resize();
		mContent[mSize - 1] = amount;
	}

	void takeSip(int amount) {
		int liquidAmount = calculateLiquidAmount();
		if (liquidAmount > 0 && amount > 0) {
			if (liquidAmount <= amount) {
				spill();
			}
		} else {
			for (size_t i = 0; i < mSize; i++) {
				float liquidRatio = (float) mContent[i]->getAmount()
						/ (float) liquidAmount;
				mContent[i]->remove(liquidRatio * amount);
			}
		}
	}

	void spill() {
		for (size_t i = 0; i < mSize; i++) {
			mContent[i]->removeAll();
		}
		clear();
	}

//	cup & operator +=(const cup & object) {
//		cup(this->mContent += object.mContent());
//		return *this;
//}

	void clear() {
		for (size_t i = 0; i < mSize; i++) {
			delete mContent[i];
		}
		delete[] mContent;
		mContent = 0;
		mSize = 0;
	}
};

#endif /* CUP_HPP_ */
