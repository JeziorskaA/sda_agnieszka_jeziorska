/*
 * espresso.hpp
 *
 *  Created on: 08.05.2017
 *      Author: Agnieszka
 */

#ifndef ESPRESSO_HPP_
#define ESPRESSO_HPP_

#include "caffee.hpp"
class espresso: public caffee {
public:
	espresso(int amount) :
			caffee(amount, 120) {
	}

	void add(int amount) {
		mAmount += amount;

	}
	void remove(int amount) {
		mAmount = (mAmount > amount) ? mAmount - amount : 0;
	}
	void removeAll() {
		mAmount = 0;
	}
};

#endif /* ESPRESSO_HPP_ */
