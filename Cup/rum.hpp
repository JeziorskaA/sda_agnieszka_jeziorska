/*
 * rum.hpp
 *
 *  Created on: 08.05.2017
 *      Author: Agnieszka
 */

#ifndef RUM_HPP_
#define RUM_HPP_
#include "liquid.hpp"

enum Colour {
	light, dark, no_colour
};

class rum: public liquid {
protected:
	Colour mColour;
public:
	rum() :
			mColour(no_colour) {
	}
	rum(int amount, Colour colour) :
			liquid(amount), mColour(colour) {
	}
	Colour getColour() const {
		return mColour;
	}
	void add(int amount) {
		mAmount += amount;

	}
	void remove(int amount) {
		mAmount = (mAmount > amount) ? mAmount - amount : 0;
	}
	void removeAll() {
		mAmount = 0;
	}

};

#endif /* RUM_HPP_ */
