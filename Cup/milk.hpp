/*
 * milk.hpp
 *
 *  Created on: 08.05.2017
 *      Author: Agnieszka
 */

#ifndef MILK_HPP_
#define MILK_HPP_
#include "liquid.hpp"

class milk: public liquid {
protected:
	float mFat;
public:
	milk() :
			mFat(0.0) {
	}
	milk(int amount, float fat) :
			liquid(amount), mFat(fat) {
	}

	float getFat() const {
		return mFat;
	}

	void add(int amount) {
		mAmount += amount;

	}
	void remove(int amount) {
		mAmount = (mAmount > amount) ? mAmount - amount : 0;
	}
	void removeAll() {
		mAmount = 0;
	}

};

#endif /* MILK_HPP_ */
