/*
 * main.cpp
 *
 *  Created on: 06.05.2017
 *      Author: Agnieszka
 */

#include "liquid.hpp"
#include "milk.hpp"
#include "rum.hpp"
#include "caffee.hpp"
#include "espresso.hpp"
#include "cup.hpp"
#include <iostream>
using namespace std;

int main() {

	cup KubekKawyZMlekiem;
	KubekKawyZMlekiem.add(new caffee(250,0));
	KubekKawyZMlekiem.add(new milk (100, 2.0));
	KubekKawyZMlekiem.wypisz();
	return 0;
}

