/*
 * main.cpp
 *
 *  Created on: 01.04.2017
 *      Author: Agnieszka
 */

#include "Lista.hpp"

int main()
{
	Lista lista;
	lista.dodajPoczatek(7);
	lista.dodajPoczatek(45);
	lista.dodajPoczatek(56);
	lista.dodajPoczatek(35);
	lista.dodajPoczatek(2);
	lista.wypisz();
	return 0;
}

