/*
 * Lista.hpp
 *
 *  Created on: 01.04.2017
 *      Author: Agnieszka
 */

#ifndef LISTA_HPP_
#define LISTA_HPP_
#include <iostream>

using namespace std;
class Lista
{
private:
	class Wezel
	{
	public:
		int mWartosc;
		Wezel* nast;
		Wezel(int wartosc) // konstruktor klasy Wezel
		{
			mWartosc = wartosc;
			nast = 0;
		}
	};

	Wezel* head; // wska�nik na pierwszy element
	Wezel* tail; // wska�nik na ostatni element
public:
	Lista() // konstruktor Listy
	{
		head = 0;
		tail = 0;
	}
	void wypisz()
	{
		Wezel* tmp = 0;
		tmp->nast = head->nast; // wska�nik ten ma wskazywac na pierwszy element

		cout << "[";
		while (tmp != 0)
		{
			cout << "[" << tmp->mWartosc << "]";
			tmp = tmp->nast;
		}
		cout << "]" << endl;
	}
	void dodajPoczatek(int wartosc)
	{
		if (head != 0)
		{
			Wezel* tmp = new Wezel(wartosc); // tworz� nowy wezel i przypisuje go od tymczasowego wska�nika
			tmp->nast = head; // nowy wezel ma wskazywac na czo�o listy
			head->nast = tmp; // czolo listy ma wskazywac na nowy wezel
		}
		else
		{
			head = new Wezel(wartosc); // stworzy now weze� i przypisze go do head, nast�pnie przypisze head to tail
			tail = head;
		}
	}
};
#endif /* LISTA_HPP_ */
