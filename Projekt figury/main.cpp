#include <iostream>
 class Figura
 {
   public:
     virtual float pole() const
     {
       return -1.0;
     }
     virtual ~Figura(){};
 };

 class Prostokat : public Figura
 {
   public:
	 Prostokat( const float a, const float b )
   	   : mA( a )
   	   , mB(b) {}

     float pole() const
     {
       return mA * mB;
     }
   private:
     float mA;
     float mB;
 };

 class Kolo : public Figura
 {
   public:
     Kolo( const float promien ) : mR( promien ) {}

     float pole() const
     {
       return pi * mR * mR;
     }
   private:
     float mR; // promien kola
     const float pi = 3.14159;
 };

 class Trojkat : public Figura
 {
   public:
	 Trojkat( const float a, const float h )
   	   : mA(a)
   	   , mH(h) {}

     float pole() const
     {
       return 0.5 * mA * mH;
     }
   private:
     float mA;
     float mH;
 };

 void wyswietlPole( Figura &figura )
 {
   std::cout << figura.pole() << std::endl;
   return;
 }

 int main()
 {
   // deklaracje obiektow:
   Figura jakasFigura;
   Prostokat jakisProstokat( 5 , 10);
   Kolo jakiesKolo( 3 );
   Trojkat jakisTrojkat(4, 7.9);

   Figura *wskJakasFigura = 0; // deklaracja wska�nika

   // obiekty -------------------------------
   std::cout << jakasFigura.pole() << std::endl;
   std::cout << jakisProstokat.pole() << std::endl;
   std::cout << jakiesKolo.pole() << std::endl;
   std::cout << jakisTrojkat.pole() << std::endl;

   // wskazniki -----------------------------
   wskJakasFigura = &jakasFigura;
   std::cout << wskJakasFigura->pole() << std::endl;
   wskJakasFigura = &jakisProstokat;
   std::cout << wskJakasFigura->pole() << std::endl;
   wskJakasFigura = &jakiesKolo;
   std::cout << wskJakasFigura->pole() << std::endl;
   wskJakasFigura = &jakisTrojkat;
   std::cout << wskJakasFigura->pole() << std::endl;

   // referencje -----------------------------
   wyswietlPole( jakasFigura );
   wyswietlPole( jakisProstokat );
   wyswietlPole( jakiesKolo );
   wyswietlPole( jakisTrojkat );

   return 0;
 }
