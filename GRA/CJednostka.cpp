/*
 * CJednostka.cpp
 *
 *  Created on: Mar 25, 2017
 *      Author: orlik
 */

#include "CJednostka.hpp"
#include <iostream>

int CJednostka::getAtak() const
{
	return mAtak;
}

char CJednostka::getSymbol() const
{
	return mSymbol;
}

int CJednostka::getSzybkosc() const
{
	return mSzybkosc;
}

int CJednostka::getX() const
{
	return mX;
}

void CJednostka::setX(int x)
{
	mX = x;
}

int CJednostka::getY() const
{
	return mY;
}

void CJednostka::setY(int y)
{
	mY = y;
}

int CJednostka::getZycie() const
{
	return mZycie;
}

CJednostka::CJednostka(int x, int y, char symbol, int zycie, int atak,
		int szybkosc) :
		mX(x), mY(y), mSymbol(symbol), mZycie(zycie), mAtak(atak), mSzybkosc(
				szybkosc)
{
}

bool CJednostka::ruch(int x, int y)
{
	mX = x;
	mY = y;
	return true;
}

bool CJednostka::atak(int x, int y)
{
	std::cout << "Ciach!\n";
	return true;
}
