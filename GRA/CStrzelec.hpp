/*
 * CStrzelec.hpp
 *
 *  Created on: Mar 25, 2017
 *      Author: orlik
 */

#ifndef CSTRZELEC_HPP_
#define CSTRZELEC_HPP_

#include "CJednostka.hpp"

class CStrzelec: public CJednostka
{
public:
	CStrzelec(char symbol = '>', int zycie = 180, int atak = 90, int szybkosc =
			80);
	bool atak(int x, int y);
};

#endif /* CSTRZELEC_HPP_ */
