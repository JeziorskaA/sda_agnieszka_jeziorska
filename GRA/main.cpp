#include "CWojownik.hpp"
#include "CKawaleria.hpp"
#include "CStrzelec.hpp"
#include <iostream>

int main()
{
	CStrzelec strzelec;
	CKawaleria kawaleria;
	CWojownik wojownik;

	strzelec.atak(1, 1);
	wojownik.atak(1, 1);
	kawaleria.atak(1, 1);

	strzelec.CJednostka::atak(1, 1);
	wojownik.CJednostka::atak(1, 1);
	kawaleria.CJednostka::atak(1, 1);
	return 0;
}
