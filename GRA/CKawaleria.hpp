/*
 * CKawaleria.hpp
 *
 *  Created on: Mar 25, 2017
 *      Author: orlik
 */

#ifndef CKAWALERIA_HPP_
#define CKAWALERIA_HPP_

#include "CJednostka.hpp"

class CKawaleria: public CJednostka
{
public:
	CKawaleria(char symbol = '%', int zycie = 180, int atak = 100,
			int szybkosc = 20);

	bool atak(int x, int y);

};

#endif /* CKAWALERIA_HPP_ */
