/*
 * CJednostka.hpp
 *
 *  Created on: Mar 25, 2017
 *      Author: orlik
 */

#ifndef CJEDNOSTKA_HPP_
#define CJEDNOSTKA_HPP_
#include <iostream>
class CJednostka
{
protected:
	int mX;
	int mY;
	char mSymbol;
	int mZycie;
	int mAtak;
	int mSzybkosc;

public:
	bool ruch(int x, int y);
	bool atak(int x, int y);

	CJednostka(int x, int y, char symbol, int zycie, int atak, int szybkosc);
	int getAtak() const;
	char getSymbol() const;
	int getSzybkosc() const;
	int getX() const;
	void setX(int x);
	int getY() const;
	void setY(int y);
	int getZycie() const;
};

#endif /* CJEDNOSTKA_HPP_ */
