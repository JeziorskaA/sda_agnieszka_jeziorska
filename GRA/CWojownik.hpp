/*
 * CWojownik.hpp
 *
 *  Created on: Mar 25, 2017
 *      Author: orlik
 */

#ifndef CWOJOWNIK_HPP_
#define CWOJOWNIK_HPP_

#include "CJednostka.hpp"

class CWojownik: public CJednostka
{
public:
	CWojownik(char symbol = '$', int zycie = 150, int atak = 70, int szybkosc =
			40);
	bool atak(int x, int y);
};

#endif /* CWOJOWNIK_HPP_ */
