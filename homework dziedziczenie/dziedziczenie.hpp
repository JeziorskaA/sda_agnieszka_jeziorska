#ifndef DZIEDZICZENIE_HPP_
#define DZIEDZICZENIE_HPP_
#include <iostream>
using namespace std;
class Poczatek
{
protected:
	int mX;
public:
	void przedstawSie()
	{
		cout << "Jestem Poczatek \n";
	}
	Poczatek() :
			mX(0)
	{
		cout << "Poczatek\n";
	}
	Poczatek(int x) :
			mX(x)
	{
		cout << "Poczatek(x)\n";
	}
	~Poczatek()
	{
		cout << "~Poczatek\n";
	}
	void ustawX()
	{
		mX = 1;
	}
	void wypiszX()
	{
		std::cout << "Poczatek::X=" << mX << endl;
	}
};
class Srodek: public Poczatek
{
protected:
	double mX;
public:
	void przedstawSie()
	{
		cout << "Jestem Srodek \n";
	}
	Srodek() :
			Poczatek(99), mX(10.31)
	{
		cout << "Srodek\n";
	}
	Srodek(double x) :
			Poczatek(33), mX(x)
	{
		cout << "Srodek(x)\n";
	}
	~Srodek()
	{
		cout << "~Srodek\n";
	}
	void ustawX()
	{
		Poczatek::mX = 11;
		mX = 12.31;
	}
	void wypiszX()
	{
		std::cout << "Poczatek::X=" << Poczatek::mX << "Srodek::X=" << mX
				<< endl;
	}
};
class Koniec: public Srodek
{
protected:
	int mX;
public:
	void przedstawSie()
	{
		cout << "Jestem Koniec \n";
	}
	Koniec() :
			Srodek(3213.333), mX(100)
	{
		cout << "Koniec\n";
	}
	Koniec(int x) :
			Srodek(3213.333), mX(x)
	{
		cout << "Koniec(x)\n";
	}
	~Koniec()
	{
		cout << "~Koniec\n";
	}
	void ustawX()
	{
		Poczatek::mX = 111;
		Srodek::Poczatek::mX = 112.432;
		mX = 113;
	}
	void wypiszX()
	{
		std::cout << "Poczatek::X=" << Poczatek::mX << "Srodek::X="
				<< Srodek::mX << "Koniec::X=" << mX << endl;
	}
};
#endif /* DZIEDZICZENIE_HPP_ */
