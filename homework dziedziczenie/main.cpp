#include <iostream>
#include "dziedziczenie.hpp"

int main()
{
	Poczatek *pocz = new Poczatek(4); // wywoła konstruktor z parametrem
	Srodek *srod = new Srodek(222.12); //wywoła konstruktor z parametrem dla klasy Srodek i dla klasy Poczatek
	Koniec *koni = new Koniec(421); //wywoła konstruktor z parametrem dla klasy Koniec, Srodek i Poczatek

	pocz->przedstawSie(); //wywołanie metody przedstaw się dla OBIEKTU klasy Poczatek
	srod->przedstawSie(); //wywołanie metody przedstaw się dla OBIEKTU klasy Srodek
	koni->przedstawSie(); //wywołanie metody przedstaw się dla OBIEKTU klasy Koniec

	koni->Poczatek::przedstawSie(); //wywołanie metody przedstaw się z zasięgu Poczatek dla OBIEKTU klasy Koniec
	//ZAD wywołanie metody przedstaw się z zasięgu Srodek dla OBIEKTU klasy Koniec
	//ZAD wywołanie metody przedstaw się z zasięgu Poczatek dla OBIEKTU klasy Srodek

	//analogicznie do przykładów wyżej
	pocz->wypiszX();
	srod->wypiszX();
	koni->wypiszX();

	pocz->ustawX();
	srod->ustawX();
	koni->ustawX();

	pocz->wypiszX();
	srod->wypiszX();
	koni->wypiszX();

	//ZAD Wywołaj metody ustawX i wypiszX dla pozostałych zasięgów (dla klasy Koniec z Srodek i Początek, dla klasy Srodek z Poczatek)

	delete pocz; //zniszczenie obiektu. Wywoła konstruktor
	delete srod; //zniszczenie obiektu. Wywoła konstruktor
	delete koni; //zniszczenie obiektu. Wywoła konstruktor

	//Wszystkie przykłady powyżej będą też działały dla obiektów nie tworzonych dynamicznie:

	Srodek srodeczek(3232.222);
	srodeczek.przedstawSie();
	srodeczek.wypiszX();
	srodeczek.ustawX();
	srodeczek.wypiszX();

	return 0;
}
