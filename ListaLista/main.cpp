/*
 * main.cpp
 *
 *  Created on: 01.04.2017
 *      Author: Agnieszka
 */

#include "Lista.hpp"

int main()
{
	Lista lista;
	lista.dodajPoczatek(7);
	lista.dodajPoczatek(45);
	lista.dodajPoczatek(56);
	lista.dodajPoczatek(35);
	lista.dodajPoczatek(2);
	lista.wypisz();
	lista.dodajKoniec(-9);
	lista.dodajKoniec(-34);
	lista.dodajKoniec(-49);
	lista.dodajKoniec(-8);
	lista.dodajKoniec(-1);
	lista.dodajPoczatek(177);
	lista.wypisz();
	std::cout << "Znaleziona wartosc: " << lista.pobierz(5) << std::endl;
	std::cout << "Nieznaleziona wartosc: " << lista.pobierz(565) << std::endl;

	lista.wyczysc();
	lista.wypisz();

	lista.znajdzPozycje(45);

	return 0;
}

