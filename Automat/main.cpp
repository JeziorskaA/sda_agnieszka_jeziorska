/*
 * main.cpp
 *
 *  Created on: 08.04.2017
 *      Author: Agnieszka
 */

#include <iostream>
using namespace std;
class Pieniadz
{
private:
	double mWartosc;
	int mIlosc;
};
int main()
{
	const int iloscNominaow = 15;
	double nominaly[iloscNominaow] =
	{ 500.0, 200.0, 100.0, 50.0, 20.0, 10.0, 5.0, 2.0, 1.0, 0.5, 0.2, 0.1, 0.05,
			0.02, 0.01 };
	double reszta = 21.50;
	int i = 0;
	while (reszta > 0 && i < iloscNominaow)
	{
		if (reszta >= nominaly[i])
		{
			int ileRazyWydac = reszta / nominaly[i];
			reszta = reszta - (nominaly[i] * ileRazyWydac);
			cout << nominaly[i] << " x " << ileRazyWydac << endl;
		}
		i++;
	}
	return 0;
}

