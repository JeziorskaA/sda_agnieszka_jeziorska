/*
 * DataCzas.hpp
 *
 *  Created on: 30.03.2017
 *      Author: Agnieszka
 */

#ifndef DATACZAS_HPP_
#define DATACZAS_HPP_
#include "Data.hpp"
#include "Czas.hpp"
class DataCzas: public Data, public Czas
{
public:
	DataCzas();
	DataCzas(unsigned int dzien, unsigned int miesiac, unsigned int rok, int godziny, int minuty, int sekundy);
	DataCzas(Data, Czas);
	void wypisz();
	void przesunDataCzas(DataCzas a);
	void przesunDataCzas(unsigned int dzien, unsigned int miesiac, unsigned int rok, int godziny, int minuty, int sekundy);
};
#endif /* DATACZAS_HPP_ */
