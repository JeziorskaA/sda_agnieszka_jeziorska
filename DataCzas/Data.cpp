/*
 * Data.cpp
 *
 *  Created on: 28.03.2017
 *      Author: Agnieszka
 */

#include "Data.hpp"
Data::Data() :
		mDzien(0), mMiesiac(0), mRok(0)
{
}
Data::Data(unsigned int dzien, unsigned int miesiac, unsigned int rok)
{
	setDzien(dzien);
	setMiesiac(miesiac);
	setRok(rok);
}
unsigned int Data::getDzien() const
{
	return mDzien;
}
void Data::setDzien(unsigned int dzien)
{
	if (dzien < 1)
	{
		mDzien = 1;
	}
	else if (dzien > 31)
	{
		mDzien = 31;
	}
	else
		mDzien = dzien;
}
unsigned int Data::getMiesiac() const
{
	return mMiesiac;
}
void Data::setMiesiac(unsigned int miesiac)
{
	if (miesiac < 1)
	{
		mMiesiac = 1;
	}
	else if (miesiac > 12)
	{
		mMiesiac = 12;
	}
	else
		mMiesiac = miesiac;
}
unsigned int Data::getRok() const
{
	return mRok;
}
void Data::setRok(unsigned int rok)
{
	if (rok < 1900)
	{
		mRok = 1900;
	}
	else if (rok > 2050)
	{
		mRok = 2050;
	}
	else
		mRok = rok;
}
void Data::wypisz()
{
	cout << "Dzie�: " << mDzien << " ,miesi�c: " << mMiesiac << " ,rok: "
			<< mRok << endl;
}
void Data::przesunDateL(unsigned int lata)
{
	mRok += lata;
}
void Data::przesunDateM(unsigned int miesiace)
{
	unsigned int nowyMiesiac = mMiesiac + miesiace;
	mMiesiac = nowyMiesiac % 12;
	unsigned int nowyRok = (nowyMiesiac - mMiesiac) / 12;
	mRok += nowyRok;
}
void Data::przesunDateD(unsigned int dni)
{
	unsigned int nowyDzien = mDzien + dni;
	mDzien = nowyDzien % 31;
	unsigned int mMMiesiac = (nowyDzien - mDzien) / 31;
	unsigned int nowyMiesiac = mMiesiac + mMMiesiac;
	mMiesiac = nowyMiesiac % 12;
	unsigned int nowyRok = (nowyMiesiac - mMiesiac) / 12;
	mRok += nowyRok;
}
