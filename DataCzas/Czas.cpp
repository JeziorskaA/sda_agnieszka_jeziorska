/*
 * Czas.cpp
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#include "Czas.hpp"
#include <iostream>
using namespace std;
Czas::Czas() :
		mGodziny(0), mMinuty(0), mSekundy(0)
{
}
Czas::Czas(int godziny, int minuty, int sekundy)
{
	setGodziny(godziny);
	setMinuty(minuty);
	setSekundy(sekundy);
}
int Czas::getGodziny() const
{
	return mGodziny;
}
void Czas::setGodziny(int godziny)
{
	if (godziny > 23)
	{
		godziny -= 24;
	}
	else
		mGodziny = godziny;
}
int Czas::getMinuty() const
{
	return mMinuty;
}
void Czas::setMinuty(int minuty)
{
	if (minuty > 59)
	{
		minuty -= 60;
		mGodziny += 1;
	}
	else
		mMinuty = minuty;
}
int Czas::getSekundy() const
{
	return mSekundy;
}
void Czas::setSekundy(int sekundy)
{
	if (sekundy > 59)
	{
		sekundy -= 60;
		mMinuty += 1;
	}
	else
		mSekundy = sekundy;
}
void Czas::wypisz()
{
	cout << "Czas: " << mGodziny << " godziny " << mMinuty << " minuty "
			<< mSekundy << " sekundy " << endl;
}
void Czas::przesunG(int godzin)
{
	mGodziny += godzin;
}
void Czas::przesunM(int minut)
{
	int nowaMinuta = mMinuty + minut;
	mMinuty = nowaMinuta % 60;
	int minutyGodziny = (nowaMinuta - mMinuty) / 60;
	mGodziny += minutyGodziny;
}
void Czas::przesunS(int sekund)
{
	int nowaSekunda = mSekundy + sekund;
	mSekundy = nowaSekunda % 60;
	int sekundyMinuty = (nowaSekunda - mSekundy) / 60;
	mMinuty += sekundyMinuty;
	int sekundyGodziny = (nowaSekunda - mSekundy) / 60;
	mGodziny += sekundyGodziny;
}
