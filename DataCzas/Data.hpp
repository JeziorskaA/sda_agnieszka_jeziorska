/*
 * Data.hpp
 *
 *  Created on: 28.03.2017
 *      Author: Agnieszka
 */

#ifndef DATA_HPP_
#define DATA_HPP_
#include <iostream>
using namespace std;
class Data
{
protected:
	unsigned int mDzien;
	unsigned int mMiesiac;
	unsigned int mRok;
public:
	Data();
	Data(unsigned int dzien, unsigned int miesiac, unsigned int rok);
	void wypisz();
	void przesunDateL(unsigned int lata);
	void przesunDateM(unsigned int miesiace);
	void przesunDateD(unsigned int dni);
	unsigned int getDzien() const;
	void setDzien(unsigned int dzien);
	unsigned int getMiesiac() const;
	void setMiesiac(unsigned int miesiac);
	unsigned int getRok() const;
	void setRok(unsigned int rok);
};

#endif /* DATA_HPP_ */
