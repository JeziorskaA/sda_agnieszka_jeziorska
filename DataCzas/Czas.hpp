/*
 * Czas.hpp
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef CZAS_HPP_
#define CZAS_HPP_

class Czas
{
protected:
	int mGodziny;
	int mMinuty;
	int mSekundy;
public:
	Czas(int godziny, int minuty, int sekundy);
	Czas();
	void wypisz();
	int getGodziny() const;
	void setGodziny(int godziny);
	int getMinuty() const;
	void setMinuty(int minuty);
	int getSekundy() const;
	void setSekundy(int sekundy);
	void przesunG(int godzin);
	void przesunM(int minut);
	void przesunS(int sekund);
};

#endif /* CZAS_HPP_ */
