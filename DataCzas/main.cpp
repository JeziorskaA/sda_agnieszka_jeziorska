/*
 * main.cpp
 *
 *  Created on: 28.03.2017
 *      Author: Agnieszka
 */

#include"DataCzas.hpp"
#include <iostream>
using namespace std;
int main()
{
	Data pierwsza(12, 12, 2010);
	Data druga(50, 12, 80000);
	Data trzecia(0, 50, 800);
	Data czwarta(13, 12, 2000000);
	pierwsza.wypisz();
	druga.wypisz();
	trzecia.wypisz();
	czwarta.wypisz();
	pierwsza.przesunDateL(1);
	pierwsza.wypisz();
	pierwsza.przesunDateM(2);
	pierwsza.wypisz();
	pierwsza.przesunDateD(4);
	pierwsza.wypisz();
	DataCzas datadata(1, 2, 2014, 2, 30, 40);
	datadata.wypisz();
	cout << "==========================================" << endl;
	DataCzas data2(1, 1, 2010, 1, 10, 10);
	datadata.wypisz();
	cout << "==========================================" << endl;
	datadata.przesunDataCzas(data2);
	data2.wypisz();
	cout << "==========================================" << endl;
	datadata.wypisz();
	return 0;
}


