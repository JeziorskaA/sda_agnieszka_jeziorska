/*
 * Pies.hpp
 *
 *  Created on: 03.04.2017
 *      Author: Agnieszka
 */

#ifndef PIES_HPP_
#define PIES_HPP_
#include "DzikieZwierze.hpp"
class Pies: virtual public DzikieZwierze
{
public:
	Pies();
	Pies(string imie);
	void dajGlos();
};
#endif /* PIES_HPP_ */
