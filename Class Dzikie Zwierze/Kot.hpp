/*
 * Kot.hpp
 *
 *  Created on: 03.04.2017
 *      Author: Agnieszka
 */

#ifndef KOT_HPP_
#define KOT_HPP_
#include "DzikieZwierze.hpp"
class Kot: virtual public DzikieZwierze
{
public:
	Kot();
	Kot(string imie);
	void dajGlos();
};
#endif /* KOT_HPP_ */
