/*
 * main.cpp
 *
 *  Created on: 03.04.2017
 *      Author: Agnieszka
 */

#include "DzikieZwierze.hpp"
#include "Kot.hpp"
#include "Pies.hpp"
#include "KotoPies.hpp"
#include <iostream>
#include <string>
using namespace std;
int main()
{
	DzikieZwierze *zwierze[3];
	zwierze[0] = new Kot("Mruczek");
	zwierze[1] = new Pies("Burek");
	zwierze[2] = new KotoPies("B&M");

	for (int i = 0; i < 3; i++)
	{
		zwierze[i]->dajGlos();
	}
	for (int i = 0; i < 3; i++)
	{
		delete zwierze[i];
	}
	return 0;
}

