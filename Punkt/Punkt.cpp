/*
 * Punkt.cpp
 *
 *  Created on: 28.03.2017
 *      Author: Agnieszka
 */
#include "Punkt.hpp"
#include "KolorowyPunkt.hpp"
#include <cmath>

Punkt::Punkt() :
		mX(0), mY(0)
{
}

Punkt::Punkt(int x, int y) :
		mX(x), mY(y)
{
}
void Punkt::podajWspolrzedne()
{
	cout << "Wsp�rz�dne[x,y]: " << "[" << mX << " , " << mY << "]" << endl;
}

void Punkt::przesun(int x, int y)
{
	przesunX(x);
	przesunX(y);

}
void Punkt::przesun(Punkt p)
{
	przesun(p.getX(), p.getY());

}
void Punkt::przesunX(int x)
{
	mX += x;
	cout << "Wsp�rz�dne[x,y]: " << "[" << mX << " , " << mY << "]" << endl;
}
void Punkt::przesunY(int y)
{
	mY += y;
	cout << "Wsp�rz�dne[x,y]: " << "[" << mX << " , " << mY << "]" << endl;
}
void Punkt::obliczOdleglosc(Punkt a)
{
	obliczOdleglosc(a.getX(), a.getY());

}

void Punkt::obliczOdleglosc(int x, int y)
{
	int dx = mX - x;
	int dY = mY - y;
	int wynik=0;
	wynik = dx*dx + dY*dY;


	cout << "Odleg�o�� wynosi:"<< sqrt(wynik)<< endl;
}
