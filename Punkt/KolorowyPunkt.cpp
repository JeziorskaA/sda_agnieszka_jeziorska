/*
 * KolorowyPunkt.cpp
 *
 *  Created on: 28.03.2017
 *      Author: Agnieszka
 */
#include "KolorowyPunkt.hpp"

#include <string>
#include <iostream>

using namespace std;

KolorowyPunkt::KolorowyPunkt(int x, int y, Kolor kolor) :
		Punkt(x, y), mKolor(kolor)
{
}

void KolorowyPunkt::podajWspolrzedne()
{
	cout << "Wsp�rz�dne[x,y]: " << "[" << mX << " , " << mY << "]" <<"Kolor = "<<getKolorString()<< endl;

}
string KolorowyPunkt::getKolorString()
{
	string kolorek;
	switch (mKolor)
	{
	case czarny:
		kolorek = "czarny";
		break;
	case bialy:
		kolorek = "bia�y";
		break;
	case zielony:
		kolorek = "zielony";
		break;
	default:
		kolorek = "nijaki";
		break;
	}
	return kolorek;
}
