/*
 * KolorowyPunkt.hpp
 *
 *  Created on: 28.03.2017
 *      Author: Agnieszka
 */

#ifndef KOLOROWYPUNKT_HPP_
#define KOLOROWYPUNKT_HPP_
#include<string>
#include "Punkt.hpp" // bo dziedziczy z klasy punkt
using namespace std;
class KolorowyPunkt: public Punkt
{

public:
	enum Kolor
	{
		czarny,
		bialy,
		zielony
	};

	void podajWspolrzedne();
	KolorowyPunkt(int x, int y, Kolor kolor);
	string getKolorString();

protected:
	Kolor mKolor;

};



#endif /* KOLOROWYPUNKT_HPP_ */
