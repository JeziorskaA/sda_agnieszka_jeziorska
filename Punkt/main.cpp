/*
 * main.cpp
 *
 *  Created on: 28.03.2017
 *      Author: Agnieszka
 */

#include "Punkt.hpp"
#include "KolorowyPunkt.hpp"


int main()
{
	Punkt p1(2, 3);
	Punkt p2(1, 3);
	p1.podajWspolrzedne();
	p1.przesunX(2);
	p1.przesun(1, 5);
	Punkt p3(5, 5);
	p1.obliczOdleglosc(p2);

	return 0;
}
