/*
 * Student.cpp
 *
 *  Created on: 27.03.2017
 *      Author: Agnieszka
 */
#include "Student.hpp"
#include "Osoba.hpp"

CStudent::CStudent(int nrIndeksu, string kierunek) :
		mNrIndeksu(nrIndeksu), mKierunek(kierunek)
{
}
CStudent::CStudent(int nrIndeksu, string kierunek, int pesel) :
		COsoba(pesel), mNrIndeksu(nrIndeksu), mKierunek(kierunek)
{
}

void CStudent::podajNumer()
{
	cout << "Numer indeksu to: " << mNrIndeksu << endl;
}

