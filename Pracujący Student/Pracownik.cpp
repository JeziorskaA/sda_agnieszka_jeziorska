/*
 * Pracownik.cpp
 *
 *  Created on: 27.03.2017
 *      Author: Agnieszka
 */
#include "Pracownik.hpp"

CPracownik::CPracownik(int pensja, string zawod) :
		mPensja(pensja), mZawod(zawod)
{
}
CPracownik::CPracownik(int pensja, string zawod, int pesel) :
		COsoba(pesel), mPensja(pensja), mZawod(zawod)
{
}
void CPracownik::pracuj()
{
	cout << "Ci�ko pracuj�!" << endl;
}
CPracownik::~CPracownik()
{
}
;
