/*
 * Pracownik.hpp
 *
 *  Created on: 28.03.2017
 *      Author: Agnieszka
 */

#ifndef PRACOWNIK_HPP_
#define PRACOWNIK_HPP_
#include "Osoba.hpp"
#include <iostream>
#include<string>

using namespace std;
class CPracownik: public virtual COsoba
{
private:
	int mPensja;
	string mZawod;
public:
	virtual void pracuj();

	CPracownik(int pensja, string zawod);
	CPracownik(int pensja, string zawod, int pesel);
	virtual ~CPracownik();
};

#endif /* PRACOWNIK_HPP_ */
