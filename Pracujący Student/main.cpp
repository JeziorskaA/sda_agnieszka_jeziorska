/*
 * main.cpp
 *
 *  Created on: 27.03.2017
 *      Author: Agnieszka
 */
#include "PracujacyStudent.hpp"
#include "Osoba.hpp"
#include <iostream>

int main()
{
	cout << "Student" << endl;
	CStudent Marian(54875, "Finanse");
	Marian.podajNumer();
	cout << "Pracownik" << endl;
	CPracownik Rysiek(1600, "Kopacz", 2655);
	Rysiek.pracuj();
	cout << "Pracuj�cy student" << endl;
	CPracujacyStudent JesteStudente(54769, "Ekonomia", 3500, "Ekonomista",
			64725);
	JesteStudente.pracuj();
	JesteStudente.podajNumer();
	Marian.podajPesel();
	Rysiek.podajPesel();

	CPracownik *wskPrac;
	CStudent *wskStudent;

	Rysiek.pracuj();
	wskPrac = &Rysiek;
	wskPrac->pracuj();

	Marian.podajNumer();
	wskStudent = &Marian;
	wskStudent->podajNumer();
	return 0;
}

