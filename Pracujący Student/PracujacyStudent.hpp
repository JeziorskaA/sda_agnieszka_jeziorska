/*
 * PracujacyStudent.hpp
 *
 *  Created on: 27.03.2017
 *      Author: Agnieszka
 */

#ifndef PRACUJACYSTUDENT_HPP_
#define PRACUJACYSTUDENT_HPP_
#include <iostream>
#include <string>
#include "Student.hpp"
#include "Pracownik.hpp"

using namespace std;
class CPracujacyStudent: public CStudent, public CPracownik
{
public:
	void pracuj();
	void podajNumer();
	CPracujacyStudent(int nrIndeksu, string kierunek, int pensja, string zawod,
			int pesel);
};

#endif /* PRACUJACYSTUDENT_HPP_ */
