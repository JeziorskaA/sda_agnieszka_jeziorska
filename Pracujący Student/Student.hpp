/*
 * Student.hpp
 *
 *  Created on: 27.03.2017
 *      Author: Agnieszka
 */

#ifndef STUDENT_HPP_
#define STUDENT_HPP_
#include "Osoba.hpp"
#include <iostream>
#include <string>
using namespace std;
class CStudent: public virtual COsoba
{
private:
	int mNrIndeksu;
	string mKierunek;
public:
	void podajNumer();
	CStudent(int nrIndeksu, string kierunek);
	CStudent(int nrIndeksu, string kierunek, int pesel);

};

#endif /* STUDENT_HPP_ */
