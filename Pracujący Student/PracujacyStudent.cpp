/*
 * PracujacyStudent.cpp
 *
 *  Created on: 27.03.2017
 *      Author: Agnieszka
 */

#include "PracujacyStudent.hpp"

CPracujacyStudent::CPracujacyStudent(int nrIndeksu, string kierunek, int pensja,
		string zawod, int pesel) :
		COsoba(pesel), CStudent(nrIndeksu, kierunek), CPracownik(pensja, zawod)
{
}
void CPracujacyStudent::pracuj()
{
	cout << "Studiuje i pracuje" << endl;
}
void CPracujacyStudent::podajNumer()
{
	cout << "Sesja idzie a pracy tyle...!" << endl;
}
