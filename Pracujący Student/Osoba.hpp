/*
 * Osoba.hpp
 *
 *  Created on: 28.03.2017
 *      Author: Agnieszka
 */

#ifndef OSOBA_HPP_
#define OSOBA_HPP_

#include <iostream>
using namespace std;

class COsoba
{
private:
	int mPesel;
public:
	COsoba(int pesel = 0);
	void podajPesel();

};
#endif /* OSOBA_HPP_ */
