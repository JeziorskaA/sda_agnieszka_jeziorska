/*
 * mojalista.hpp
 *
 *  Created on: 26.04.2017
 *      Author: Agnieszka
 */

#ifndef MOJALISTA_HPP_
#define MOJALISTA_HPP_
#include <iostream>
using namespace std;

class list
{
private:
	class node
	{
	public:
		int mValue;
		node *next;

		node(int value)
		{
			mValue = value;
			next = 0;
		}

	};
	node *head;
	node * tail;
public:
	list()
	{
		head = 0;
		tail = 0;
	}
	void show()
	{
		node *tmp = 0;
		tmp = head;
		while (tmp != 0)
		{
			cout << "[" << tmp->mValue << "]" << flush;
			tmp = tmp->next;
		}

	}
	void addOutset(int value)
	{
		if (head != 0)
		{
			node *tmp = new node(value);
			tmp->next = head;
			head = tmp;
		}
		else
		{
			head = new node(value);
			tail = head;
		}
	}
	void deleteOutset()
	{
		node *tmp = head;
		if (tmp)
		{
			head = tmp->next;
			delete tmp;
		}
	}
	void addTheEnd(int value)
	{
		if (head == 0)
		{
			head = new node(value);
			tail = head;
		}
		else
		{
			node *tmp = new node(value);
			tail->next = tmp;
			tail = tmp;
		}
	}
	void deleteTheEnd()
	{
		node *tmp = head;
		if(tmp != 0)
		  {
		    if(tail)
		    {
		      while((tmp->next)->next) tmp = tmp->next;
		      delete tmp->next;
		      tmp->next = NULL;
		    }
		    else
		    {
		      delete tmp;    // lista jednoelementowa
		      head = NULL; // staje si� list� pust�
		    }
		  }
		}
	int pobierz( int index)
		{
			node * tmp = head;
			int counter = 0;
			while(tmp != 0)
			{
				counter ++;
				if(counter == index)
				{
					return tmp -> mValue;

				}else
				{
					tmp = tmp->next;
				}
			}
			return 0;

		}

};

#endif /* MOJALISTA_HPP_ */
