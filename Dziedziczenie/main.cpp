/*
 * main.cpp
 *
 *  Created on: Mar 23, 2017
 *      Author: orlik
 */
#include <iostream>
#include "dziedziczenie.hpp"
int main()
{
	Poczatek *pocz = new Poczatek();
	Srodek *srod = new Srodek();
	Koniec *koni = new Koniec();
	pocz->przedstawSie();
	srod->przedstawSie();
	koni->przedstawSie();
	koni->Poczatek::przedstawSie();
	pocz->wypiszX();
	srod->wypiszX();
	koni->wypiszX();
	pocz->ustawX();
	srod->ustawX();
	koni->ustawX();
	pocz->wypiszX();
	srod->wypiszX();
	koni->wypiszX();
	pocz->wypiszX();
	delete pocz;
	delete srod;
	delete koni;
	return 0;
}
