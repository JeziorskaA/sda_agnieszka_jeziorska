/*
 * Data.hpp
 *
 *  Created on: 27.03.2017
 *      Author: Agnieszka
 */

#ifndef DATA_HPP_
#define DATA_HPP_
#include <iostream>

using namespace std;
class Data
{
private:
	unsigned int mDzien;
	unsigned int mMiesiac;
	unsigned int mRok;
public:
	Data(unsigned int dzien, unsigned int miesiac, unsigned int rok);
};

#endif /* DATA_HPP_ */
