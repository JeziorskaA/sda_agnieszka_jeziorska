/*
 * Cylinder.hpp
 *
 *  Created on: 18.04.2017
 *      Author: Agnieszka
 */

#ifndef CYLINDER_HPP_
#define CYLINDER_HPP_
#include "Shape.hpp"

class Cylinder: public Shape
{
private:
	float mH;
public:
	virtual float area();
	virtual void name();
	Cylinder();
	Cylinder(float r, float h);
	virtual ~Cylinder();
};



#endif /* CYLINDER_HPP_ */
