/*
 * main.cpp
 *
 *  Created on: 18.04.2017
 *      Author: Agnieszka
 */
#include "Shape.hpp"
#include "Circle.hpp"
#include "Cylinder.hpp"

int main()
{
Circle circle1(2.5);
circle1.area();
circle1.name();
Cylinder cylinder1 (5.1, 7.2);
cylinder1.area();
cylinder1.name();
std::cout<< "=.=.=.=.=.=.=.=.=.=.=.=.=.=.=" << std::endl;
std::cout<< std::endl;
Shape *shape1[10];
shape1[0] = new Circle (2.5);
shape1[1] = new Cylinder (5.1,7.2);
shape1[2] = new Circle (7.5);
shape1[3] = new Cylinder (5.8,1.0);
shape1[4] = new Circle (5.4);
shape1[5] = new Cylinder (3.1,4.2);
shape1[6] = new Circle (2.4);
shape1[7] = new Cylinder (3.1,1.7);
shape1[8] = new Circle (1.9);
shape1[9] = new Cylinder (2.6,4.2);

for(int i = 0; i<10; i++)
{
	shape1[i] ->area();
	shape1[i] ->name();
}
for(int i = 0; i<10; i++)
{
	delete shape1[i];
}
return 0;
}



