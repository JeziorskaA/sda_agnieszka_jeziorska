/*
 * Shape.hpp
 *
 *  Created on: 18.04.2017
 *      Author: Agnieszka
 */

#ifndef SHAPE_HPP_
#define SHAPE_HPP_
#include <iostream>
class Shape
{
protected:
	float mR;
	float mPi = 3.14;
public:
	virtual float area()=0;
	virtual void name()=0;
	Shape();
	Shape(float r);
	virtual ~Shape();
};

#endif /* SHAPE_HPP_ */
