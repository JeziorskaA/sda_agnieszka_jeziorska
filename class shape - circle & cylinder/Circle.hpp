/*
 * Circle.hpp
 *
 *  Created on: 18.04.2017
 *      Author: Agnieszka
 */

#ifndef CIRCLE_HPP_
#define CIRCLE_HPP_
#include "Shape.hpp"
#include<iostream>
class Circle: public Shape
{
public:
	virtual float area();
	virtual void name();
	Circle();
	Circle(float r);
	virtual ~Circle();

};

#endif /* CIRCLE_HPP_ */
