/*
 * Cylinder.cpp
 *
 *  Created on: 18.04.2017
 *      Author: Agnieszka
 */
#include "Cylinder.hpp"

Cylinder::Cylinder()
{
	mH = 0;
}
Cylinder::Cylinder(float r, float h) :
		Shape(r)
{
	mH = h;
}

float Cylinder::area()
{
	float score = 0.0;
	score = 2 * (mPi * mR * mR) + 2 * (mPi * mR * mH);
	std::cout << "The area of the cylinder is: " << score << std::endl;
	return score;
}
void Cylinder::name()
{
	std::cout << "I'm the Cylinder !" << std::endl;
}

Cylinder::~Cylinder()
{
}

