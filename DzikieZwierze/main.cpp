/*
 * main.cpp
 *
 *  Created on: 01.04.2017
 *      Author: Agnieszka
 */

#include "DzikieZwierze.hpp"
#include "Kot.hpp"
#include "Pies.hpp"
#include "KotoPies.hpp"
int main()
{
	Kot Mruczek("Mruczek ");
	Pies Burek("Burek ");

	DzikieZwierze *wsk;
	wsk = &Burek;
	wsk->dajGlos();

	wsk = &Mruczek;
	wsk->dajGlos();

	DzikieZwierze* zwierze[6];
	zwierze[0] = new Pies("Ares ");
	zwierze[1] = new Pies("Rufus ");
	zwierze[2] = new Kot("Filemon ");
	zwierze[3] = new Kot("Kitka ");
	zwierze[4] = new KotoPies("KitkaAres ");
	zwierze[5] = new KotoPies("FilemonRufus ");

	for (int i = 0; i < 6; i++)
	{
		zwierze[i]->dajGlos();
	}

	for (int i = 0; i < 6; i++)
	{
		delete zwierze[i];
	}
	return 0;
}

