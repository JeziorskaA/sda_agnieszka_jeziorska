/*
 * DzikieZwierze.hpp
 *
 *  Created on: 01.04.2017
 *      Author: Agnieszka
 */

#ifndef DZIKIEZWIERZE_HPP_
#define DZIKIEZWIERZE_HPP_
#include <iostream>
#include <string>
using namespace std;
class DzikieZwierze
{
protected:
	string mImie;
public:
	virtual void dajGlos()=0;
	DzikieZwierze();
	DzikieZwierze(string imie);
	virtual ~DzikieZwierze();
};

#endif /* DZIKIEZWIERZE_HPP_ */
