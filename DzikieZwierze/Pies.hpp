/*
 * Pies.hpp
 *
 *  Created on: 01.04.2017
 *      Author: Agnieszka
 */

#ifndef PIES_HPP_
#define PIES_HPP_
#include "DzikieZwierze.hpp"
class Pies: virtual public DzikieZwierze
{
public:
	void dajGlos();
	Pies(string imiePsa);
	Pies();
};
#endif /* PIES_HPP_ */
