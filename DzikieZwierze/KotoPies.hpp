/*
 * KotoPies.hpp
 *
 *  Created on: 01.04.2017
 *      Author: Agnieszka
 */

#ifndef KOTOPIES_HPP_
#define KOTOPIES_HPP_
#include "Kot.hpp"
#include "Pies.hpp"
class KotoPies: public Kot, public Pies
{
public:
	KotoPies(string imie);
	void dajGlos();
};
#endif /* KOTOPIES_HPP_ */
