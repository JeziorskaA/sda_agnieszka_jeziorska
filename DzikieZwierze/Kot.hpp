/*
 * Kot.hpp
 *
 *  Created on: 01.04.2017
 *      Author: Agnieszka
 */

#ifndef KOT_HPP_
#define KOT_HPP_
#include "DzikieZwierze.hpp"
class Kot: virtual public DzikieZwierze
{
public:
	void dajGlos();
	Kot(string imieKota);
	Kot();
};
#endif /* KOT_HPP_ */
