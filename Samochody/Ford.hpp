/*
 * Ford.hpp
 *
 *  Created on: 27.03.2017
 *      Author: Agnieszka
 */

#ifndef FORD_HPP_
#define FORD_HPP_
#include <iostream>
#include "Samochod.hpp"
using namespace std;

class CFord: public CSamochod
{
public:
	CFord(string kolor = "czarny", string marka = "Ford", int pojemnosc = 2000);
	virtual ~CFord(){};
	virtual void podajKolor();
	virtual void podajMarke();
	virtual void podajPojemnoscSilnika();
};

#endif /* FORD_HPP_ */
