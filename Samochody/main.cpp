/*
 * main.cpp
 *
 *  Created on: 27.03.2017
 *      Author: Agnieszka
 */

#include <iostream>
#include "Samochod.hpp"
#include "Audi.hpp"
#include "Ford.hpp"

int main()
{
	CSamochod* tablica[2];
	tablica[0] = new CAudi("Srebrny", "A7", 2000);
	tablica[1] = new CFord("Czarny", "Mondeo", 2000);
	tablica[2] = new CFord("Bia�y", "Galaxy", 2000);

	for (int i = 0; i < 3; ++i)
	{
		tablica[i]->podajKolor();
		tablica[i]->podajMarke();
		tablica[i]->podajPojemnoscSilnika();
	}
	for (int i = 0; i < 3; ++i)
	{
		delete tablica[i];
	}
	return 0;
}
