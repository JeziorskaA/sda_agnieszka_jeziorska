/*
 * Samochody.hpp
 *
 *  Created on: 27.03.2017
 *      Author: Agnieszka
 */
#ifndef Samochod_HPP_
#define Samochod_HPP_
#include <iostream>
#include <string>
using namespace std;
class CSamochod
{
protected:
	string mKolor;
	string mMarka;
	int mPojemnosc;
public:
	virtual void podajKolor()=0;
	virtual void podajMarke()=0;
	virtual void podajPojemnoscSilnika()=0;
	CSamochod(string kolor, string marka, int pojemnosc);
	virtual ~CSamochod(){};
};
#endif /* AUDI_HPP_ */
