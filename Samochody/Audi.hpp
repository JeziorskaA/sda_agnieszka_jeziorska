/*
 * Audi.hpp
 *
 *  Created on: 27.03.2017
 *      Author: Agnieszka
 */

#ifndef AUDI_HPP_
#define AUDI_HPP_
#include <iostream>
#include "Samochod.hpp"
using namespace std;

class CAudi: public CSamochod
{
public:
	CAudi(string kolor = "czerwony", string marka = "Audi",
			int pojemnosc = 1800);
	virtual ~CAudi(){};
	virtual void podajKolor();
	virtual void podajMarke();
	virtual void podajPojemnoscSilnika();
};
#endif /* AUDI_HPP_ */
