/*
 * main.cpp
 *
 *  Created on: 08.04.2017
 *      Author: Agnieszka Jeziorska
 */

#include <iostream>
#include <string>
using namespace std;

class Procesor
{
protected:
	int mSerialNumber;
public:
	virtual void procesuj()=0;
	virtual ~Procesor()
	{
		cout << "~Procesor" << endl;
	}
};
class ProcesorAMD: public Procesor
{
public:
	void procesuj()
	{
		cout << "ADM PROCESSOR" << endl;
	}
	~ProcesorAMD()
	{
		cout << "~Procesuj AMD" << endl;
	}
};
class ProcesorINTEL: public Procesor
{
public:
	void procesuj()
	{
		cout << "INTEL PROCESSOR" << endl;
	}
	~ProcesorINTEL()
	{
		cout << "~ProcesorINTEL " << endl;
	}
};

class Cooler
{
public:
	virtual void cool()=0;
	virtual ~Cooler()
	{
	};
};
class CoolerAMD: public Cooler
{
public:
	void cool()
	{
		cout << "AMD COOLER" << endl;
	}
	~CoolerAMD()
	{
	};
};

class CoolerINTEL: public Cooler
{
public:
	void cool()
	{
		cout << "INTEL COOLER" << endl;
	}
	~CoolerINTEL()
	{
	};
};
class ASFactory
{
public:
	virtual Procesor* createProcessor()=0;
	virtual Cooler* createCooler()=0;
	virtual ~ASFactory()
	{
	};
};

class ASFactoryIntel: public ASFactory
{
public:
	Procesor* createProcessor()
	{
		return new ProcesorINTEL();
	}
	Cooler* createCooler()
	{
		return new CoolerINTEL();
	}

	~ASFactoryIntel()
	{
	};
};
class ASFactoryADM: public ASFactory
{
public:
	Procesor* createProcessor()
	{
		return new ProcesorAMD();
	}
	Cooler* createCooler()
	{
		return new CoolerAMD();
	}

	~ASFactoryADM()
	{
	};
};

class Computer
{
private:
	string mName;
	Procesor *mProcesor;
	Cooler *mCooler;
public:
	void run();
	Computer(string name, ASFactory &factory) :
			mName(name)
	{
		mProcesor = factory.createProcessor();
		mCooler = factory.createCooler();
	}
	~Computer()
	{
		delete mProcesor;
		delete mCooler;
	}
};

void Computer::run()
{
	cout << "Nazywam si�: " << mName << endl;
	mProcesor->procesuj();
	mCooler->cool();
}

int main()
{
	ASFactoryIntel intelFactory;
	ASFactoryADM admFactory;

	Computer intelPC("PC1", intelFactory);
	Computer admPC("PC2", admFactory);

	intelPC.run();
	admPC.run();

	return 0;
}

