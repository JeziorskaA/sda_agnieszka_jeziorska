#include <iostream>

class Zwierze
{
public:
	Zwierze(): poleWiek()
	{
	}

	Zwierze(int argumentWiek): poleWiek(argumentWiek) // konstruktor inicjalizujacy obiekt
	{
	}

protected:
	int poleWiek;
};

class Pies: public Zwierze
{
public:
	Pies(): /*Zwierze(),*/ poleWaga()
	{
	}

	Pies(int wiek, float argumentWaga)
		// lista inicjalizacyjna
		: Zwierze(wiek), poleWaga(argumentWaga)
	{
		// cia�o konstruktora:
		// tutaj nie zawo�am konstruktora klasy bazowej
	}

	void hau() const
	{
		std::cout << "Jestem Azor,\nmam " << poleWiek << " lat,\nwa�� " << poleWaga << "kg." << std::endl;
	}

private:
	float poleWaga;
};

int main()
{
	Pies azor(10, 30.0f);

	azor.hau();

	Pies reksio;

	reksio.hau();

//	int wiek;
//	wiek = 10;

//	int wiek = 10;

	return 0;
}
