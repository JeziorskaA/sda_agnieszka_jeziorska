/*
 * main.cpp
 *
 *  Created on: Mar 23, 2017
 *      Author: orlik
 */

#include <iostream>

#include "CKostka.hpp"
using namespace std;
int main()
{
	CKostka k10(10);
	CKostka k6(6);
	CKostka k20(20);

	cout << "K10= " << (k10.getWartosc()) << " K6= " << (k6.getWartosc())
			<< " K20= " << (k20.getWartosc()) << endl;

	return 0;
}
