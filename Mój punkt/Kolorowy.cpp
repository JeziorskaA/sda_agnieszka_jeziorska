/*
 * Kolorowy.cpp
 *
 *  Created on: 29.03.2017
 *      Author: Agnieszka
 */

#include "Kolorowy.hpp"

#include <string>
#include <iostream>

using namespace std;
Kolorowy::Kolorowy(int x, int y, Kolor kolor) :
		Punkt(x, y), mKolor(kolor)
{
}
void Kolorowy::wypisz()
{
	cout << "Wsp�rz�dne [" << mX << " , " << mY << "]" << "Kolor:"
			<< getKolorString() << endl;
}
string Kolorowy::getKolorString()
{
	string kolorek("");
	switch (mKolor)
	{
	case czerwony:
		kolorek = "czerwony";
		break;
	case niebieski:
		kolorek = "niebieski";
		break;
	case zielony:
		kolorek = "zielony";
		break;
	default:
		kolorek = "Nie ma takiego";
		break;
	}
	return kolorek;
}
