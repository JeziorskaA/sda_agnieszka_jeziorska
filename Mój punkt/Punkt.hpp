/*
 * Punkt.hpp
 *
 *  Created on: 29.03.2017
 *      Author: Agnieszka
 */

#ifndef PUNKT_HPP_
#define PUNKT_HPP_
#include <iostream>
#include <cmath>
using namespace std;

class Punkt
{
protected:
	// gdy dziedziczymy po tej klasie atrybuty musza byc protected!
	int mX;
	int mY;
public:
	Punkt();
	Punkt(int x, int y);
	int getX() const
	{
		return mX;
	}
	void setX(int x)
	{
		mX = x;
	}
	int getY() const
	{
		return mY;
	}
	void setY(int y)
	{
		mY = y;
	}
	void wypisz();
	void przesun(Punkt a);
	void przesun(int x, int y);
	void przesunX(int x);
	void przesunY(int y);
	void obliczOdleglosc(Punkt a);
	void obliczOdleglosc(int x, int y);
};
#endif /* PUNKT_HPP_ */
