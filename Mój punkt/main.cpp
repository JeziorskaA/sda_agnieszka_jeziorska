/*
 * main.cpp
 *
 *  Created on: 29.03.2017
 *      Author: Agnieszka
 */
#include "Punkt.hpp"
#include"Kolorowy.hpp"

int main()
{
	Punkt p1(1, 5);
	p1.wypisz();
	Punkt p2(8, 4);
	p2.wypisz();
	p1.przesun(p2);
	p1.wypisz();
	p1.przesunX(4);
	p1.wypisz();
	p2.przesunY(1);
	p2.wypisz();
	p1.obliczOdleglosc(p2);
	Kolorowy jakispunkt(1, 2, Kolorowy::zielony);
	jakispunkt.wypisz();

	return 0;
}

