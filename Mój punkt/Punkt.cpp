/*
 * Punkt.cpp
 *
 *  Created on: 29.03.2017
 *      Author: Agnieszka
 */
#include "Punkt.hpp"
#include "Kolorowy.hpp"
#include <cmath>
Punkt::Punkt() :
		mX(0), mY(0)
{
}
Punkt::Punkt(int x, int y) :
		mX(x), mY(y)
{
}
void Punkt::wypisz()
{
	cout << "Wsp�rz�dne [" << mX << " , " << mY << "]" << endl;
}
void Punkt::przesun(Punkt a)
{
	przesun(a.getX(), a.getY());
}
void Punkt::przesun(int x, int y)
{
	przesunX(x);
	przesunY(y);
}
void Punkt::przesunX(int x)
{
	mX += x;
}
void Punkt::przesunY(int y)
{
	mY += y;
}
void Punkt::obliczOdleglosc(Punkt a)
{
	obliczOdleglosc(a.getX(), a.getY());
}
void Punkt::obliczOdleglosc(int x, int y)
{
	int Dx = mX - x;
	int Dy = mY - y;

	cout << "Odleg�o�c punktu wynosi: " << sqrt(Dx * Dx + Dy * Dy) << endl;
}
