/*
 * Kolorowy.hpp
 *
 *  Created on: 29.03.2017
 *      Author: Agnieszka
 */

#ifndef KOLOROWY_HPP_
#define KOLOROWY_HPP_
#include "Punkt.hpp"
#include <string>
#include<iostream>
using namespace std;
class Kolorowy: public Punkt
{
public:
	enum Kolor
	{
		czerwony, niebieski, zielony
	};
	void wypisz();

	Kolorowy(int x, int y, Kolor kolor);
	string getKolorString();
protected:
	Kolor mKolor;
};
#endif /* KOLOROWY_HPP_ */
